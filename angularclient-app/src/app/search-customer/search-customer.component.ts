import { Component, OnInit } from '@angular/core';
import { Customer } from '../customer';
import { CustomerServiceService } from '../customer-service.service';

@Component({
  selector: 'app-search-customer',
  templateUrl: './search-customer.component.html',
  styleUrls: ['./search-customer.component.css']
})
export class SearchCustomerComponent implements OnInit {

  customers:Customer[];
  constructor(private customerService:CustomerServiceService) { }

  ngOnInit() {

  }

  age:number;

  onSubmit()
  {
    this.searchCustomers();
  }

  submitted=false;
  searchCustomers() {
    this.customers = [];
    this.customerService.getCustomersByAge(this.age)
      .subscribe(
        data => {
          this.customers=data;
          this.submitted = true;
        },
      );
  }
}
