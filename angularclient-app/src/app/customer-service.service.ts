import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CustomerServiceService {

  constructor(private http:HttpClient) {

   }
   private baseUrl = 'http://localhost:8082/api/customers';
 
   createCustomer(customer: any): Observable<any> {
       return this.http.post(this.baseUrl, customer);
     }

     getCustomers(): Observable<any> {
      return this.http.get(this.baseUrl);
    }

    getCustomer(id: number): Observable<any> {
      return this.http.get(`${this.baseUrl}/${id}`);
    }

    updateCustomer(customer: Object): Observable<Object> {
      return this.http.put(`${this.baseUrl}` + `/update`, customer);
     }
    
deleteCustomer(id: number): Observable<any> {​​​​​​​​
return this.http.delete(`${​​​​​​​​this.baseUrl}​​​​​​​​/${​​​​​​​​id}​​​​​​​​`);
  }​​​​​​​​

  getCustomersByAge(age: number): Observable<any> {
    return this.http.get(`${this.baseUrl}/age/${age}`);
  }

}
