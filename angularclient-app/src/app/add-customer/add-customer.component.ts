import { Component, OnInit } from '@angular/core';
import { Customer } from '../customer';
import { CustomerServiceService } from '../customer-service.service';

@Component({
  selector: 'app-add-customer',
  templateUrl: './add-customer.component.html',
  styleUrls: ['./add-customer.component.css']
})
export class AddCustomerComponent implements OnInit {

  customer:Customer=new Customer();
  submitted=false;

  constructor(private customerService:CustomerServiceService) { 

  }


  ngOnInit() {
  }

  onSubmit()
  {
    console.log("Successfully Submitted");
    this.save();
  }

  save() {
    this.customerService.createCustomer(this.customer)
      .subscribe(
        data => {
          console.log(data);
          this.submitted = true;
        },
        error => console.log(error));
    this.customer = new Customer();
  }

  newCustomer(): void {
    this.submitted = false;
    this.customer = new Customer();
  }
}
