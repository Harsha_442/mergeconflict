import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Customer } from '../customer';
import { CustomerServiceService } from '../customer-service.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-customer-list',
  templateUrl: './customer-list.component.html',
  styleUrls: ['./customer-list.component.css']
})
export class CustomerListComponent implements OnInit {

  customers:Observable<Customer[]>;
  constructor(private customerService:CustomerServiceService,private router:Router) { }

  ngOnInit() {
    this.reloadData();
  }

  reloadData()
  {
     this.customers=this.customerService.getCustomers();
  }

  editCustomer(customer: Customer):void {
    console.log("into edit");
    localStorage.setItem("id",customer.id.toString());
    this.router.navigate(["edit"]);
  }

  deleteCustomer(customer:Customer) {
    this.customerService.deleteCustomer(customer.id)
      .subscribe(
        data => {
          console.log(data);
         this.reloadData();
        
        },
        error => console.log(error));
  }
}
