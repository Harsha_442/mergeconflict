import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AddCustomerComponent } from './add-customer/add-customer.component';
import { CustomerListComponent } from './customer-list/customer-list.component';
import { EditCustomerComponent } from './edit-customer/edit-customer.component';
import { SearchCustomerComponent } from './search-customer/search-customer.component';
import { LoginComponent } from './login/login.component';
import { AuthguardService } from './authguard.service';
import { LogoutComponent } from './logout/logout.component';


const routes: Routes = [
  {
    path:'add',
    component:AddCustomerComponent 
  },
  {
    path:'customer',
    component:CustomerListComponent,
    canActivate:[AuthguardService]
  },
  {
    path:'edit',
    component:EditCustomerComponent
  },
  {
    path:'search',
    component:SearchCustomerComponent
  },
  {
    path:'login',
    component:LoginComponent
  },
  {
    path:'',
    redirectTo:'customer',
    pathMatch:'full'
  },
  {
    path:'logout',
    component:LogoutComponent,
    canActivate:[AuthguardService]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
